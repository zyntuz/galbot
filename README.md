# Galbot

A group game chatbot built with [NoneBot](https://github.com/richardchien/nonebot) which works with QQ, a chinese im.

This is an initiative to learn about python and basic chatbot development.

## Run

```bash
pip install -r requirements.txt
python run.py
```

See NoneBot's documentation for details.

### Setup

Please add yourself to `SUPERUSERS` in `config.py`. Then send `init` to galbot to create a sqlite database.

## Features

A list of commands for players can be found at https://zyntuz.github.io/galbot.

Daily signup, work, steal, planet (buy, sell, manage, etc), dice, coin flip, ranking, etc.
