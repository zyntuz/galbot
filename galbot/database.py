import datetime
from typing import Union

from peewee import *
# from playhouse.migrate import *

database = SqliteDatabase('database.db', pragmas={'foreign_keys': 1})


class BaseModel(Model):
    class Meta:
        database = database


class Player(BaseModel):
    id = BigIntegerField(unique=True, primary_key=True)
    nickname = CharField(unique=True)
    exp = BigIntegerField(default=0)
    credit = BigIntegerField(default=200)
    resource = BigIntegerField(default=50)
    max_planets = IntegerField(default=2)
    action_time = DateTimeField(default=datetime.datetime.now())
    income_date = DateField(default=datetime.date.today() - datetime.timedelta(days=1))

    @staticmethod
    def exists(identifier: Union[int, str]):
        if isinstance(identifier, int):
            return Player.select().where(Player.id == identifier).exists()
        else:
            return Player.select().where(Player.nickname == identifier).exists()


class StealRecord(BaseModel):
    attacker = ForeignKeyField(Player, backref='attacker')
    victim = ForeignKeyField(Player, backref='victim')
    amount = IntegerField()


class Planet(BaseModel):
    owner = ForeignKeyField(Player, backref='planets')
    type = IntegerField(default=1)
    size = IntegerField(default=1)
    name = CharField(default='无名')
    level = IntegerField(default=1)
    population = IntegerField(default=5)
    resource = IntegerField(default=0)
    economy = IntegerField(default=0)


# class ShipModel(BaseModel):
#     name = CharField()
#     base_armor = IntegerField()
#     base_shield = IntegerField()
#     base_supply = IntegerField()
#     slots = IntegerField()
#
#
# class Ship(BaseModel):
#     owner = ForeignKeyField(Player, backref='ships')
#     model = ForeignKeyField(ShipModel)
#     operation = IntegerField()
#     armor = IntegerField()
#     supply = IntegerField()


def create_tables():
    with database:
        database.create_tables([Player, StealRecord])


# migrator = SqliteMigrator(database)
# resource_field = BigIntegerField(default=50)
# max_planets_field = IntegerField(default=2)


def migrate_call():
    with database:
        database.drop_tables([Planet])
        database.create_tables([Planet])

    # migrate(
    #     migrator.add_column('player', 'resource', resource_field),
    #     migrator.add_column('player', 'max_planets', max_planets_field),
    # )
