from typing import Callable

from peewee import DoesNotExist
from nonebot import CommandSession

from .database import Player


def inject_account(func: Callable) -> Callable:
    async def wrapper(session: CommandSession):
        if session.state.get('player'):
            return await func(session)

        try:
            player = Player.get_by_id(session.ctx['user_id'])
        except DoesNotExist:
            session.finish('大佬请先[注册]！')
            return
        session.state['player'] = player
        return await func(session)

    return wrapper


def inject_planet(func: Callable) -> Callable:
    async def wrapper(session: CommandSession):
        if session.state.get('planet'):
            return await func(session)

        player = session.state['player']
        planets = list(player.planets)
        if not planets:
            session.finish('大佬请先[买球]！')
            return

        arg = session.current_arg_text.strip()
        if not arg:
            session.finish('大佬请标注球号')
            return
        try:
            choice = int(arg)
        except ValueError:
            session.finish('请使用数字')
            return
        if not 0 < choice <= len(planets):
            session.finish('选择不在范围内')
            return

        session.state['planet'] = planets[choice - 1]

        return await func(session)

    return wrapper
