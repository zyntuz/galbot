from nonebot import on_command, CommandSession
from nonebot import on_natural_language, NLPSession, IntentCommand

from .legion import *


@on_command('finder_upgrade')
async def finder_upgrade(session: CommandSession):
    content = session.get('content')
    msg = upgrade(content)
    await session.send(msg)


@on_natural_language(keywords=('合成', '升到'))
async def _(session: NLPSession):
    msg = session.msg_text.strip()
    return IntentCommand(80.2, 'finder_upgrade', {'content': msg})


@on_command('finder_check')
async def finder_check(session: CommandSession):
    content = session.get('content')
    msg = check(content)
    await session.send(msg)


@on_natural_language(keywords=('分', '多少钱'))
async def _(session: NLPSession):
    msg = session.msg_text.strip()
    return IntentCommand(80.1, 'finder_check', {'content': msg})
