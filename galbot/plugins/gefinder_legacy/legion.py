import math

upgfees = [[300, 75, 150], [57600, 14400, 28800], [3456000, 864000, 1728000]]
rateucp = 30000
modrates = [[40, 10, 20], [7800, 1950, 3900], [512000, 128000, 256000]]

items = [
    ['丙', '核心'], ['甲', '盾', '防空', '货', '仓'],
    ['乙', '武器', '钻头', '引擎', '能源', '喷子', '磁轨', '激光']
]
sizes = [['S', 's'], ['M', 'm'], ['L', 'l']]
errors = [
    '无法识别组件大小！', '无法识别组件名称！'
    '无法识别组件等级！'
]


def nbformat(num):
    if num > 50000000:
        return '{}亿'.format(
            round((float(num)/100000000), 2)
        )
    elif num > 50000:
        return '{}万'.format(
            round((float(num)/10000), 1)
        )
    else:
        return str(int(round(num)))


def _readmod(content):
    if any(s in content for s in sizes[0]):
        size = 0
    elif any(s in content for s in sizes[1]):
        size = 1
    elif any(s in content for s in sizes[2]):
        size = 2
    else:
        size = -1

    if any(s in content for s in items[0]):
        item = 0
    elif any(s in content for s in items[1]):
        item = 1
    elif any(s in content for s in items[2]):
        item = 2
    else:
        item = -1

    for i in range(12, 0, -1):
        if str(i) in content:
            level = i
            content = content.replace(str(i), '')
            break
    if level == 1:
        level = -1

    for i in range(12, 0, -1):
        base = i
        if str(i) in content:
            content = content.replace(str(i), '')
            break

    return [size, item, level, base]


def upgrade(content):
    r = _readmod(content)

    for i in range(3):
        if r[i] == -1:
            return errors[-1]

    fee = round(upgfees[r[0]][r[1]] * math.pow(2, r[2] - 2) * (r[2] - r[3]))
    feef = nbformat(fee)
    feelp = nbformat(fee / rateucp)
    return '从' + str(r[3]) + '级升到' + str(r[2]) + '级需要:\n' + feef + ' UCP' + ' (' + sizes[r[0]][0] + items[r[1]][0] + ')\n' + "也就是 " + feelp + " 分"


def check(content):
    if '个' in content:
        amountstr = content.split('个')[0].strip()
        content = content.replace(amountstr, '')
    else:
        amountstr = '1'

    r = _readmod(content)

    for i in range(3):
        if r[i] == -1:
            return errors[-1]

    lp = int(round(modrates[r[0]][r[1]] /
                   math.pow(2, 10 - r[2]) * int(amountstr)))
    lpf = nbformat(lp)
    return amountstr + '个' + str(r[2]) + '级' + sizes[r[0]][0] + items[r[1]][0] + '需要:\n' + lpf + " 分"
