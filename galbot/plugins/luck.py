import random

from nonebot import on_command, CommandSession


@on_command('luck1', aliases=('硬币',), only_to_me=False)
async def _(session: CommandSession):
    await session.send(random.choice(['正面', '反面']))


@on_command('luck2', aliases=('色子', '骰子'), only_to_me=False)
async def _(session: CommandSession):
    await session.send(random.choice(['⚀', '⚁', '⚂', '⚃', '⚄', '⚅']))


@on_command('luck3', aliases=('随机',), only_to_me=False)
async def _(session: CommandSession):
    args = session.current_arg_text.strip().split()
    start, end = 1, 100
    try:
        if len(args) == 1:
            end = int(args[0])
        elif len(args) > 1:
            start, end = int(args[0]), int(args[1])
    except ValueError:
        session.finish('请使用阿拉伯数字~')

    if start > end:
        start, end = end, start

    if end > 1000 or start < -1000:
        session.finish('范围太大~')

    await session.send(str(random.randint(start, end)))
