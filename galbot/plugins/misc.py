import random

from nonebot import on_command, CommandSession, get_loaded_plugins
from nonebot import on_natural_language, NLPSession, IntentCommand
from nonebot import on_notice, NoticeSession


@on_notice('group_increase')
async def _(session: NoticeSession):
    if session.ctx['group_id'] != 225290171:
        return
    await session.send('欢迎新大佬～ 群地位-1\n加团还是串门呀')


@on_command('man', aliases=('帮助', '功能', '说明', '菜单'), only_to_me=False)
async def _(session: CommandSession):
    plugins = get_loaded_plugins()
    await session.send(
        '帮助文件请访问: https://zyntuz.gitlab.io/gal-help'
    )


@on_command('unknown')
async def _(session: CommandSession):
    await session.send(
        random.choice(['喵喵喵', '喵喵喵喵', '嘤嘤嘤']) +
        random.choice(['？', '??', '~？', '..?'])
    )


@on_natural_language()
async def _(session: NLPSession):
    return IntentCommand(60, 'unknown')
