import datetime

from nonebot import on_command, CommandSession, permission
from nonebot.argparse import ArgumentParser

from galbot.database import Player, create_tables, migrate_call, Planet
from .planet.common import *


@on_command('init', permission=permission.SUPERUSER)
async def _(session: CommandSession):
    create_tables()
    await session.send('Done')


@on_command('migrate', permission=permission.SUPERUSER)
async def _(session: CommandSession):
    migrate_call()
    await session.send('Done')


@on_command('rename', shell_like=True, permission=permission.SUPERUSER)
async def _(session: CommandSession):
    parser = ArgumentParser(session=session)
    parser.add_argument('old')
    parser.add_argument('new')

    args = parser.parse_args(session.argv)

    if not Player.exists(args.old):
        session.finish('Player not found')
    if Player.exists(args.new):
        session.finish('This nickname already exists')

    player = Player.get(nickname=args.old)
    player.nickname = args.new
    player.save()
    session.finish('Renamed')


@on_command('remove', shell_like=True, permission=permission.SUPERUSER)
async def _(session: CommandSession):
    parser = ArgumentParser(session=session)
    parser.add_argument('player')

    args = parser.parse_args(session.argv)

    if not Player.exists(args.player):
        session.finish('Player not found')

    player = Player.get(nickname=args.player)
    player.delete_instance()
    session.finish('Removed')


@on_command('credit', shell_like=True, permission=permission.SUPERUSER)
async def _(session: CommandSession):
    parser = ArgumentParser(session=session)
    parser.add_argument('player')
    parser.add_argument('credit', type=int)

    args = parser.parse_args(session.argv)

    if not Player.exists(args.player):
        session.finish('Player not found')

    player = Player.get(nickname=args.player)
    player.credit = args.credit
    player.save()
    session.finish(f'Credit changed to {args.credit}')


@on_command('resource', shell_like=True, permission=permission.SUPERUSER)
async def _(session: CommandSession):
    parser = ArgumentParser(session=session)
    parser.add_argument('player')
    parser.add_argument('resource', type=int)

    args = parser.parse_args(session.argv)

    if not Player.exists(args.player):
        session.finish('Player not found')

    player = Player.get(nickname=args.player)
    player.resource = args.resource
    player.save()
    session.finish(f'Resource changed to {args.resource}')


@on_command('set_action_time', aliases=('冷却',), shell_like=True, permission=permission.SUPERUSER)
async def _(session: CommandSession):
    parser = ArgumentParser(session=session)
    parser.add_argument('player')
    parser.add_argument('date')

    args = parser.parse_args(session.argv)

    if not Player.exists(args.player):
        session.finish('Player not found')

    player = Player.get(nickname=args.player)

    if args.date in ('now', '现在'):
        player.action_time = datetime.datetime.now()
        player.save()
        session.finish('Action time set')

    try:
        date = datetime.datetime.strptime(args.date, "%Y-%m-%d")
    except ValueError:
        session.finish('日期格式错误')
        return

    player.action_time = date
    player.save()
    session.finish('Action time set')


@on_command('set_income_time', aliases=('重置签到',), shell_like=True, permission=permission.SUPERUSER)
async def _(session: CommandSession):
    parser = ArgumentParser(session=session)
    parser.add_argument('player')

    args = parser.parse_args(session.argv)

    if not Player.exists(args.player):
        session.finish('Player not found')

    player = Player.get(nickname=args.player)

    player.income_date = datetime.date.today() - datetime.timedelta(1)
    player.save()
    session.finish('Income date reset')


@on_command('rm_planet', aliases=('删球',), shell_like=True, permission=permission.SUPERUSER)
async def _(session: CommandSession):
    parser = ArgumentParser(session=session)
    parser.add_argument('index', type=int)

    args = parser.parse_args(session.argv)

    planet = Planet.get_by_id(args.index)
    planet.delete_instance()

    session.finish('Planet removed')


@on_command('assets_ranking', aliases=('财富排行',), only_to_me=False, permission=permission.SUPERUSER)
async def _(session: CommandSession):
    players = []
    for p in Player.select():
        credit = p.credit + round(p.resource / 5)
        for planet in p.planets:
            ori_price: int = round(types[planet.type]['price'] * sizes[planet.size]['price'])
            credit += round((
                ori_price * 0.5 + (planet.population - 5) * 20 + upgrades[planet.level]['cost']
                + upgrades[planet.resource]['cost'] * 0.5 + upgrades[planet.economy]['cost'] * 0.5
            ) * 1.5)
        players.append((p.nickname, credit))
    players.sort(key=lambda tup: tup[1], reverse=True)

    reply = '# =财富排行='
    for i in range(10):
        reply += f'\n{i+1} {players[i][0]} ({players[i][1]})'
    reply += '========'
    for i in range(10, 12):
        reply += f'\n{i+1} {players[i][0]} ({players[i][1]})'

    await session.send(reply)
