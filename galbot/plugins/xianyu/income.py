import datetime
import random

from nonebot import on_command, CommandSession

from galbot.helpers import inject_account, Player
from .planet.common import *


@on_command('income', aliases=('签到',), only_to_me=False)
@inject_account
async def income(session: CommandSession):
    player = session.state['player']
    today = datetime.date.today()
    if (today - player.income_date).days < 1:
        session.finish('大佬今天已经签到过辣\n当前本地时间：' + datetime.datetime.now().strftime('%H:%M'))

    planets = list(player.planets)
    planet_credit = 0
    planet_res = 0
    reply: str = f'{player.nickname}大佬签到成功啦！\n'

    for planet in planets:
        credit: int = round(
            (upgrades[planet.level]['income']/2 + upgrades[planet.economy]['income'])
            * types[planet.type]['eco_buff'] * random.uniform(0.9, 1.1)
        )
        res: int = round(
            (upgrades[planet.level]['income'] / 2 + upgrades[planet.resource]['income'])
            * types[planet.type]['res_buff'] * random.uniform(0.9, 1.1)
        )
        planet_credit += credit
        planet_res += res
        r = random.random()
        reply += f'{planet.name}收益:\n{credit}信用+{res}资源\n'
        pop = await pop_calc(planet.population, planet.type, r)
        if pop > 0:
            planet.population += pop
            planet.save()
            reply += f'人口增加了{pop}\n'

    credit_reward: int = random.randint(30, 50)
    player.credit += credit_reward + planet_credit
    player.resource += planet_res
    player.income_date = today
    player.save()
    reply += f'个人奖励：{credit_reward}信用'
    session.finish(reply)


async def pop_calc(pop: int, p_type: int, r: float) -> int:
    mating_chance = pop / 20
    if mating_chance < 0.5:
        mating_chance = 0.5
    mating_chance *= types[p_type]['pop_buff']
    if mating_chance < 1:
        if mating_chance > r:
            return 1
        else:
            return 0
    return random.randint(1, mating_chance)
