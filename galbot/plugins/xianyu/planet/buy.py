from nonebot import CommandSession

from galbot.database import Player, Planet
from galbot.helpers import inject_account

from .common import *
from .expressions import *


@cg.command('buy', aliases=('买球',))
@inject_account
async def buy(session: CommandSession):
    player = session.state['player']

    price: int = round(types[1]['price'] * sizes[1]['price'])
    reply: str = f'如果想要购买极小型荒漠球 ({price}信用)，请回复[是]。如果想要自定义想买的球，请回复[否]'
    choice = session.get('choice', prompt=reply)
    if choice == '是':
        planet_size = 1
        planet_type = 1
    elif choice == '否':
        try:
            planet_size = int(session.get('size', prompt=WHICH_SIZE))
        except ValueError:
            session.finish(ERROR_FORMAT)
            return

        try:
            planet_type = int(session.get('type', prompt=WHICH_TYPE))
        except ValueError:
            session.finish(ERROR_FORMAT)
            return

        if not ((1 <= planet_size <= 5) and (1 <= planet_type <= 5)):
            session.finish(ERROR_RANGE)
            return
    else:
        session.finish('以放弃购买，拜拜~')
        return

    price: int = round(types[planet_type]['price'] * sizes[planet_size]['price'])
    reply: str = f'{sizes[planet_size]["name"]}型{types[planet_type]["name"]}球 {price}信用\n'
    if player.credit < price:
        reply += '你太穷辣，买不起诶'
        session.finish(reply)
        return

    if len(player.planets) >= player.max_planets:
        reply += '球位满啦！'
        session.finish(reply)
        return

    reply += '请问[确定]吗?'
    confirm = session.get('confirm', prompt=reply)
    if confirm != "确定":
        session.finish('以放弃购买，拜拜~')
        return

    player.credit -= price
    player.save()
    Planet.create(
        owner=player,
        type=planet_type,
        size=planet_size,
    )
    session.finish('购买成功辣！')


@buy.args_parser
async def _(session: CommandSession):
    stripped_arg = session.current_arg_text.strip().replace(' ', '')
    if session.current_key:
        session.state[session.current_key] = stripped_arg
