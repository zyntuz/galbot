from nonebot import CommandGroup

cg = CommandGroup('planet', only_to_me=False)

types = {
    1: {'name': '荒漠', 'price': 0.8, 'pop_buff': 0.6, 'eco_buff': 0.6, 'res_buff': 0.4},
    2: {'name': '海洋', 'price': 1.8, 'pop_buff': 1.0, 'eco_buff': 1.4, 'res_buff': 0.8},
    3: {'name': '类地', 'price': 2.1, 'pop_buff': 1.8, 'eco_buff': 1.0, 'res_buff': 1.0},
    4: {'name': '冰原', 'price': 2.3, 'pop_buff': 0.6, 'eco_buff': 1.8, 'res_buff': 1.2},
    5: {'name': '火焰', 'price': 1.8, 'pop_buff': 0.4, 'eco_buff': 0.6, 'res_buff': 2.2},
}

sizes = {
    1: {'name': '极小', 'price': 600, 'max_lvl': 5, 'max_pop': 20},
    2: {'name': '小', 'price': 1500, 'max_lvl': 10, 'max_pop': 80},
    3: {'name': '中', 'price': 8000, 'max_lvl': 15, 'max_pop': 250},
    4: {'name': '大', 'price': 20000, 'max_lvl': 20, 'max_pop': 750},
    5: {'name': '巨大', 'price': 90000, 'max_lvl': 30, 'max_pop': 18000},
}

upgrades = {
    0: {'cost': 0, 'pop_cost': 0, 'income': 0},
    1: {'cost': 50, 'pop_cost': 1, 'income': 90},
    2: {'cost': 200, 'pop_cost': 2, 'income': 144},
    3: {'cost': 500, 'pop_cost': 4, 'income': 216},
    4: {'cost': 1000, 'pop_cost': 6, 'income': 315},
    5: {'cost': 2000, 'pop_cost': 8, 'income': 432},
    6: {'cost': 4000, 'pop_cost': 10, 'income': 585},
    7: {'cost': 8000, 'pop_cost': 15, 'income': 774},
    8: {'cost': 10000, 'pop_cost': 20, 'income': 1035},
    9: {'cost': 20000, 'pop_cost': 25, 'income': 1260},
    10: {'cost': 30000, 'pop_cost': 30, 'income': 1530},
    11: {'cost': 40000, 'pop_cost': 40, 'income': 1800},
    12: {'cost': 50000, 'pop_cost': 50, 'income': 2640},
    13: {'cost': 75000, 'pop_cost': 60, 'income': 3520},
    14: {'cost': 100000, 'pop_cost': 80, 'income': 4400},
    15: {'cost': 125000, 'pop_cost': 100, 'income': 5280},
    16: {'cost': 150000, 'pop_cost': 125, 'income': 7040},
    17: {'cost': 175000, 'pop_cost': 150, 'income': 8800},
    18: {'cost': 200000, 'pop_cost': 175, 'income': 10560},
    19: {'cost': 250000, 'pop_cost': 200, 'income': 12320},
    20: {'cost': 300000, 'pop_cost': 300, 'income': 14080},
    21: {'cost': 400000, 'pop_cost': 400, 'income': 15840},
    22: {'cost': 500000, 'pop_cost': 500, 'income': 17600},
    23: {'cost': 600000, 'pop_cost': 600, 'income': 19800},
    24: {'cost': 800000, 'pop_cost': 800, 'income': 22000},
    25: {'cost': 1000000, 'pop_cost': 1000, 'income': 24640},
    26: {'cost': 1350000, 'pop_cost': 2000, 'income': 27280},
    27: {'cost': 1500000, 'pop_cost': 3500, 'income': 29920},
    28: {'cost': 1750000, 'pop_cost': 5000, 'income': 32560},
    29: {'cost': 2000000, 'pop_cost': 6500, 'income': 35200},
    30: {'cost': 2250000, 'pop_cost': 8000, 'income': 39600},
}

