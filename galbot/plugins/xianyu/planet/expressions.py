from .common import *

WHICH_SIZE: str = (
    '要买什么大小得呀? 括号内是基础价格\n'
    f'[1] {sizes[1]["name"]} ({sizes[1]["price"]})\n'
    f'[2] {sizes[2]["name"]} ({sizes[2]["price"]})\n'
    f'[3] {sizes[3]["name"]} ({sizes[3]["price"]})\n'
    f'[4] {sizes[4]["name"]} ({sizes[4]["price"]})\n'
    f'[5] {sizes[5]["name"]} ({sizes[5]["price"]})'
)

WHICH_TYPE: str = (
    '要买什么类型得呀? 括号内是基础价格的改动\n'
    f'[1] {types[1]["name"]} ({types[1]["price"]})\n'
    f'[2] {types[2]["name"]} ({types[2]["price"]})\n'
    f'[3] {types[3]["name"]} ({types[3]["price"]})\n'
    f'[4] {types[4]["name"]} ({types[4]["price"]})\n'
    f'[5] {types[5]["name"]} ({types[5]["price"]})'
)

ERROR_FORMAT: str = '格式错误，操作取消'
ERROR_RANGE: str = '超出范围，操作取消'
