from peewee import DoesNotExist
from nonebot import CommandSession
from nonebot.command.argfilter import extractors, validators

from galbot.database import Player, Planet
from galbot.helpers import inject_account, inject_planet

from .common import *


@cg.command('rename', aliases=('星球命名','星球改名'))
@inject_account
@inject_planet
async def rename(session: CommandSession):
    player = session.state['player']
    planet = session.state['planet']

    cost: int = 100
    filters = [
        extractors.extract_text,
        str.strip,
        validators.fit_size(1, 5, '名字长度不能大于5'),
    ]

    reply: str = (
        f'当前名字是{planet.name}。\n'
        f'改名将花费{cost}信用\n'
        f'请输入新名称...'
    )
    name = session.get('name', prompt=reply, arg_filters=filters)

    reply = f'如果确定要使用“{name}”作为昵称，请回复[确定]'
    confirm = session.get('confirm', prompt=reply)
    if confirm.strip() != "确定":
        session.finish('操作取消')
        return

    if player.credit < cost:
        session.finish('穷成酱还想改名?（嫌弃')
        return

    planet.name = name
    planet.save()
    player.credit -= cost
    player.save()

    session.finish('改好辣')


@rename.args_parser
async def _(session: CommandSession):
    stripped_arg = session.current_arg_text.strip().replace(' ', '')
    if session.current_key:
        session.state[session.current_key] = stripped_arg
