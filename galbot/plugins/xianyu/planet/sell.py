from nonebot import CommandSession

from galbot.database import Player, Planet
from galbot.helpers import inject_account, inject_planet

from .common import *
from .expressions import *


@cg.command('sell', aliases=('卖球',))
@inject_account
@inject_planet
async def buy(session: CommandSession):
    player: Player = session.state['player']
    planet: Planet = session.state['planet']

    ori_price: int = round(types[planet.type]['price'] * sizes[planet.size]['price'])
    price: int = round(
        ori_price * 0.5 + (planet.population - 5) * 20 + upgrades[planet.level]['cost']
        + upgrades[planet.resource]['cost'] * 0.5 + upgrades[planet.economy]['cost'] * 0.5
    )
    reply: str = f'[确定]要卖掉{planet.name}吗?\n估价为{price}信用。'

    confirm = session.get('confirm', prompt=reply)
    if confirm != "确定":
        session.finish('以放弃，拜拜~')
        return

    player.credit += price
    player.save()
    planet.delete_instance()
    session.finish('卖掉啦！')
