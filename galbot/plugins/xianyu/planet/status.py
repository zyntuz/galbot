from peewee import DoesNotExist
from nonebot import CommandSession

from galbot.database import Player, Planet
from galbot.helpers import inject_account

from .common import *


@cg.command('status', aliases=('星球',))
@inject_account
async def status(session: CommandSession):
    player = session.state['player']
    planets = list(player.planets)
    if not planets:
        session.finish('大佬请先[买球]！')
        return

    choice = session.state['choice']
    if choice is None:
        reply: str = player.nickname
        for i, planet in enumerate(planets):
            reply += f'\n[{i+1}] {planet.name} ({planet.level})'
        reply += '\n请使用[星球 #]查询详细信息'
        session.finish(reply)
        return

    if not 0 < choice <= len(planets):
        session.finish('选择不在范围内！')
        return

    planet = planets[choice - 1]
    reply: str = (
        f'{choice} {planet.name}\n'
        f'{sizes[planet.size]["name"]}型{types[planet.type]["name"]}球\n'
        f'等级: {planet.level}\n'
        f'人口: {planet.population}\n'
        f'资源等级: {planet.resource}\n'
        f'经济等级: {planet.economy}'
    )

    await session.send(reply)


@status.args_parser
async def _(session: CommandSession):
    stripped_arg = session.current_arg_text.strip()
    if stripped_arg:
        try:
            session.state['choice'] = int(stripped_arg)
        except ValueError:
            session.finish('请使用数字')
    else:
        session.state['choice'] = None
