from peewee import DoesNotExist
from nonebot import CommandSession
from nonebot.command.argfilter import extractors, validators

from galbot.database import Player, Planet
from galbot.helpers import inject_account, inject_planet

from .common import *


@cg.command('upgrade', aliases=('星球升级',))
@inject_account
@inject_planet
async def _(session: CommandSession):
    player: Player = session.state['player']
    planet: Planet = session.state['planet']

    reply: str = (
        f'+- {planet.name} -+\n'
        f'[星球]: {planet.level}级\n'
        f'[经济]: {planet.economy}级\n'
        f'[资源]: {planet.resource}级\n'
        f'人口: {planet.population}\n'
        '请问你要升级哪个部分?'
    )
    choice = session.get('choice', prompt=reply)
    if choice == '星球':
        attr = 'level'
        credit_cost: int = round(upgrades[planet.level + 1]['cost'] * 1.2)
        res_cost: int = round(upgrades[planet.level + 1]['cost'] * 1.2)
        pop_cost: int = round(upgrades[planet.level + 1]['pop_cost'] * 2)
    elif choice == '经济':
        attr = 'economy'
        credit_cost: int = round(upgrades[planet.economy + 1]['cost'] * 1)
        res_cost: int = round(upgrades[planet.economy + 1]['cost'] * 0.5)
        pop_cost: int = round(upgrades[planet.economy + 1]['pop_cost'])
    elif choice == '资源':
        attr = 'resource'
        credit_cost: int = round(upgrades[planet.resource + 1]['cost'] * 0.5)
        res_cost: int = round(upgrades[planet.resource + 1]['cost'] * 1)
        pop_cost: int = round(upgrades[planet.resource + 1]['pop_cost'])
    else:
        session.finish('已取消')
        return

    if getattr(planet, attr) == sizes[planet.size]['max_lvl']:
        session.finish('已经满级啦！')
        return

    if attr != 'level' and getattr(planet, attr) == planet.level:
        session.finish('设施等级不能高于星球等级诶')
        return

    reply: str = (
        f'{choice}升到{getattr(planet, attr) + 1}级需要：\n'
        f'{credit_cost}信用\n'
        f'{res_cost}资源\n'
        f'{pop_cost}人口\n'
    )
    if player.credit < credit_cost or player.resource < res_cost or planet.population < pop_cost:
        reply += '你太穷辣，升不起诶'
        session.finish(reply)
        return

    reply += '请问[确定]吗'
    confirm = session.get('confirm', prompt=reply)
    if confirm != '确定':
        session.finish('已取消')
        return

    player.credit -= credit_cost
    player.resource -= res_cost
    player.save()
    planet.population -= pop_cost
    setattr(planet, attr, getattr(planet, attr) + 1)
    planet.save()
    session.finish('升级成功')
