from nonebot import on_command, CommandSession

from galbot.database import Player


@on_command('ranking', aliases=('排行',), only_to_me=False)
async def ranking(session: CommandSession):
    reply = '# =信用排行='
    i = 0
    for p in (Player
              .select(Player.nickname, Player.credit)
              .order_by(Player.credit.desc())
              .limit(10)):
        i += 1
        reply += f'\n{i} {p.nickname} ({p.credit})'

    await session.send(reply)
