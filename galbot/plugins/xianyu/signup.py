from nonebot import on_command, CommandSession

from galbot.database import Player


@on_command('signup', aliases=('注册',), only_to_me=False)
async def signup(session: CommandSession):
    player_id = session.ctx['user_id']
    if Player.exists(player_id):
        session.finish('大佬别闹啦，你已经注册过啦！')

    nickname = session.get('nickname', prompt='请输入你的游戏昵称，要简短好记哦～')
    if Player.exists(nickname):
        session.state['nickname'] = None
        nickname = session.get('nickname', prompt='很抱歉，撞名了诶，请重新输入。')

    reply = f'如果确定要使用“{nickname}”作为昵称，请回复[确定]'
    confirm = session.get('confirm', prompt=reply)
    if confirm.strip() != "确定":
        session.state['confirm'] = None
        session.state['nickname'] = None
        nickname = session.get('nickname', prompt='那应该怎么称呼你勒？')

    Player.create(id=player_id, nickname=nickname)
    await session.send('账号注册好啦！签到一下试试吧')


@signup.args_parser
async def _(session: CommandSession):
    stripped_arg = session.current_arg_text.strip().replace(' ', '')
    if stripped_arg.startswith('注册'):
        stripped_arg = stripped_arg[2:]
    if session.current_key:
        session.state[session.current_key] = stripped_arg
    elif stripped_arg:
        session.state['nickname'] = stripped_arg
