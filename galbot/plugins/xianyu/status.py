import datetime

from nonebot import on_command, CommandSession

from galbot.database import Player
from galbot.helpers import inject_account


@on_command('status', aliases=('状态',), only_to_me=False)
@inject_account
async def status(session: CommandSession):
    player_name = session.state.get('arg', None)
    if not player_name:
        player = session.state['player']
    else:
        if not Player.exists(player_name):
            session.finish('该目标不存在诶')
        player = Player.get(nickname=player_name)

    now = datetime.datetime.now()
    if player.action_time > now:
        delta = str(player.action_time - now).split('.', 1)[0]
    else:
        delta = '已恢复'

    reply = (
        f'{player.nickname}\n'
        f'信用: {player.credit}\n'
        f'资源: {player.resource}\n'
        f'球位: {player.max_planets}\n'
        f'冷却: {delta}'
    )

    await session.send(reply)


@status.args_parser
async def _(session: CommandSession):
    stripped_arg = session.current_arg_text.strip()
    if stripped_arg:
        session.state['arg'] = stripped_arg
