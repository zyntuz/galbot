import datetime
import random
import math

from nonebot import on_command, CommandSession

from galbot.helpers import inject_account, Player


@on_command('steal', aliases=('打劫',), only_to_me=False)
@inject_account
async def steal(session: CommandSession):
    attacker = session.state['player']
    victim_name: str = session.get('victim', prompt='你要打劫谁呀～')
    if not Player.exists(victim_name):
        session.finish('该目标不存在诶')

    victim: Player = Player.get(nickname=victim_name)
    now = datetime.datetime.now()

    if victim.id == attacker.id:
        session.finish('不要酱辣！')

    if attacker.action_time > now:
        delta = str(attacker.action_time - now).split('.', 1)[0]
        session.finish(f'休息下吧，大佬～\n距离恢复还剩{delta}')

    if victim.credit < 150:
        session.finish('您的目标太穷啦，再继续下去连裤衩都要没啦~')

    r = random.random()
    if r < 0.1:
        attacker.action_time = now + datetime.timedelta(hours=4)
        attacker.save()
        session.finish(random.choice([
            f'{victim.nickname}转身对你笑了笑，然后你就什么也不知道了... (修养4小时)',
            f'警察叔叔制伏了你... (拘留4小时)'
        ]))
    elif r < 0.4:
        attacker.action_time = now + datetime.timedelta(hours=2)
        attacker.save()
        session.finish(random.choice([
            f'{victim.nickname}发现了你并和你愉♂快的玩起'
            f'了摔♂跤... (修养2小时)',
            '迷路了... (回家2小时)',
            f'平地摔♂到了对方身上... 确认过眼神，我遇上对的人～ (恋爱2小时)',
            f'{victim.nickname}突然大喊davi护体，然后你就打劫失败了... (休息2小时)'
        ]))
    else:
        amount: int = random.randint(10, 30)
        attacker.credit += amount
        attacker.save()
        victim.credit -= amount
        victim.save()
        session.finish(f'成功抢到了{amount}块钱')


@steal.args_parser
async def _(session: CommandSession):
    stripped_arg = session.current_arg_text.strip()
    if session.current_key:
        session.state[session.current_key] = stripped_arg
    elif stripped_arg:
        session.state['victim'] = stripped_arg
