import datetime
import random

from nonebot import on_command, CommandSession

from galbot.database import Player
from galbot.helpers import inject_account


@on_command('work', aliases=('打工',), only_to_me=False)
@inject_account
async def work(session: CommandSession):
    player = session.state['player']
    now = datetime.datetime.now()

    if player.action_time > now:
        delta = str(player.action_time - now).split('.', 1)[0]
        session.finish(f'大佬先休息休息～\n距离恢复还剩{delta}')

    amount: int = random.randint(60, 80)
    player.credit += amount
    player.action_time = now + datetime.timedelta(hours=8)
    player.save()
    session.finish(f'受到预付款{amount}块钱（干活8小时）')
